﻿Imports ESRI.ArcGIS.ArcMap
Imports ESRI.ArcGIS.ArcMapUI
Imports ESRI.ArcGIS.Carto
Imports ESRI.ArcGIS.Desktop
Imports ESRI.ArcGIS.Editor
Imports ESRI.ArcGIS.esriSystem
Imports ESRI.ArcGIS.Framework
Imports ESRI.ArcGIS.Geodatabase
Imports ESRI.ArcGIS.Geometry
Imports ESRI.ArcGIS.Geoprocessing

Public Class SelectCurves
    Inherits ESRI.ArcGIS.Desktop.AddIns.Button


    Public Sub SelectCurves()
        Dim pApp As ESRI.ArcGIS.Framework.IApplication = My.ArcMap.ThisApplication
        Dim pEditor As IEditor = pApp.FindExtensionByName("ESRI Object Editor")
        Dim pMap As IMap = My.Document.ActiveView.FocusMap
        Dim pLayers As IEnumLayer = pMap.Layers
        pLayers.Reset()
        Dim pLayer As ILayer = pLayers.Next
        If Not (pEditor.EditState = 1) Then
            MsgBox("Start editing first")
        End If
        Dim fLayer As IFeatureLayer = pLayer
        If (fLayer.FeatureClass.ShapeType = esriGeometryType.esriGeometryPoint) Then
            MsgBox("Current edit layer must have polylines or polygons")
        End If

        Dim fSel As IFeatureSelection = fLayer
        fSel.Clear()
        Dim fCur As IFeatureCursor
        Dim pQuery As IQueryFilter = New QueryFilter
        pQuery.WhereClause = "PARCEL_ID <> ''"
        fCur = fLayer.Search(pQuery, False)
        Dim iFeat As IFeature
        Dim pGeom As IGeometry
        iFeat = fCur.NextFeature
        pGeom = CType(iFeat.Shape, IGeometry)
        Dim pVar As Boolean

        While Not pGeom Is Nothing
            Dim segColl As ISegmentCollection
            segColl = CType(pGeom, ISegmentCollection)
            segColl.HasNonLinearSegments(pVar)
            If (pVar) Then
                fSel.Add(iFeat)
            End If
            iFeat = fCur.NextFeature
            If Not iFeat Is Nothing Then
                pGeom = CType(iFeat.Shape, IGeometry)
            Else : pGeom = Nothing
            End If
        End While
        fSel.SelectionChanged()
        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(fCur)
        My.Document.ActiveView.Refresh()

    End Sub

    Protected Overrides Sub OnClick()
        '
        SelectCurves()
        '
        My.ArcMap.Application.CurrentTool = Nothing
    End Sub

    Protected Overrides Sub OnUpdate()
        Enabled = My.ArcMap.Application IsNot Nothing
    End Sub
End Class
